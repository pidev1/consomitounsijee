package managedBeans;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import entity.Publish;
import entity.PublishComent;
import entity.user;
import service.PublishService;

@ManagedBean(name = "PublishBean", eager = true)
@SessionScoped
@JsonIgnoreProperties(ignoreUnknown = true)
public class PublishBean {

	List<Publish> listAllPublish;
	List<PublicationCommentaire> list = new ArrayList<>();
	user u ;
	@EJB
	PublishService publishService;

	@PostConstruct
	public void init() {
		u = new user();
		u.setUsersId(1);
		this.getPubs();
		if (this.listAllPublish == null) {
			this.listAllPublish = new ArrayList<>();
		}
		this.list = this.convertListBeforeJava8(this.listAllPublish);
	 
	}

	public void getPubs() {
		this.listAllPublish = publishService.GetAllPublish();
		if (this.listAllPublish == null)
			this.listAllPublish = new ArrayList<>();

	}

	public List<Publish> getListAllPublish() {
		return listAllPublish;
	}

	public void setListAllPublish(List<Publish> listAllPublish) {
		this.listAllPublish = listAllPublish;
	}

	 

	public List<PublicationCommentaire> convertListBeforeJava8(List<Publish> l) {
		List<PublicationCommentaire> list = new ArrayList<>();
		for (Publish pub : l) {
			PublishComent p = new PublishComent(pub);
			p.setPublishId(p.getPublishId());
			list.add(new PublicationCommentaire(pub, new PublishComent(pub)));
		}
		return list;
	}

	public List<PublicationCommentaire> getList() {
		return list;
	}

	public void setList(List<PublicationCommentaire> list) {
		this.list = list;
	}
	
	public void addComment(PublishComent comment) {
		comment.setMyUsers(u);
		
		this.publishService.addComment(comment,1,comment.getPublish().getPublishId());
		this.getPubs();
		if (this.listAllPublish == null) {
			this.listAllPublish = new ArrayList<>();
		}
		comment.setContent("");
		this.list = this.convertListBeforeJava8(this.listAllPublish);
		
	}

}
