package managedBeans;

import entity.Publish;
import entity.PublishComent;

public class PublicationCommentaire {

	private Publish publish;
	private PublishComent publishComent;
	public Publish getPublish() {
		return publish;
	}
	public void setPublish(Publish publish) {
		this.publish = publish;
	}
	public PublishComent getPublishComent() {
		return publishComent;
	}
	public void setPublishComent(PublishComent publishComent) {
		this.publishComent = publishComent;
	}
	public PublicationCommentaire(Publish publish, PublishComent publishComent) {
		super();
		this.publish = publish;
		this.publishComent = publishComent;
	}
	public PublicationCommentaire() {
		super();
	}
	
	
}
