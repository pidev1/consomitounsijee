package managedBeans;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import entity.DeliveryMan;
import entity.Product;
import service.DeliveyManService;
import service.ProductService;

@ManagedBean(name ="ProductBean", eager = true)
@ViewScoped 
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductBean {
	
	List<Product> lst = new ArrayList<Product>();
	@EJB
	ProductService prodService;
	
	Product prod = new Product();
	@PostConstruct
	public void init() {
	 
		lst=prodService.GetAllProduct();
	}

	public List<Product> getLst() {
		return lst;
	}

	public ProductService getProdService() {
		return prodService;
	}

	public void setProdService(ProductService prodService) {
		this.prodService = prodService;
	}

	public Product getProd() {
		return prod;
	}

	public void setProd(Product prod) {
		this.prod = prod;
	}

	public void setLst(List<Product> lst) {
		this.lst = lst;
	}
	
	
	
	

}
