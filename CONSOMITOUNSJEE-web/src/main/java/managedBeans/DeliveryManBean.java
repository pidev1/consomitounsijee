package managedBeans;

 
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import entity.DeliveryMan;
import entity.Shops;
import entity.user;
import service.DeliveyManService;

@ManagedBean(name = "DeliveryManBean", eager = true)
@ViewScoped 
@JsonIgnoreProperties(ignoreUnknown = true)
public class DeliveryManBean {
	
	List<DeliveryMan> lst = new ArrayList<DeliveryMan>();
	
	@EJB
	DeliveyManService deliveyManService;
	
	DeliveryMan deliveryMan = new DeliveryMan();
	
	@PostConstruct
	public void init() {
	 
		lst=deliveyManService.GetAllDeliveryMan();
		
	}

	public List<DeliveryMan> getLst() {
		return lst;
	}

	public void setLst(List<DeliveryMan> lst) {
		this.lst = lst;
	}

	public DeliveryMan getDeliveryMan() {
		return deliveryMan;
	}

	public void setDeliveryMan(DeliveryMan deliveryMan) {
		this.deliveryMan = deliveryMan;
	}
	
	public void addGeliberyMan() {
		this.deliveyManService.addDelivryMan(deliveryMan);
	}
	

	
}
