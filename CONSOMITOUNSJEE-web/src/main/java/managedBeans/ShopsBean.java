package managedBeans;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import entity.DeliveryMan;
import entity.Shops;
import service.DeliveyManService;
import service.ShopsService;

@ManagedBean(name ="ShopsBean", eager = true)
@ViewScoped 
@JsonIgnoreProperties(ignoreUnknown = true)
public class ShopsBean {

	
List<Shops> lst = new ArrayList<Shops>();
	
	@EJB
	ShopsService shopservice;
	
	Shops shops = new Shops();
	
	@PostConstruct
	public void init() {
		lst=shopservice.GetAllShops();
			
		
	}
	
	
	
	

	



	public List<Shops> getLst() {
		return lst;
	}









	public void setLst(List<Shops> lst) {
		this.lst = lst;
	}









	public Shops getShops() {
		return shops;
	}









	public void setShops(Shops shops) {
		this.shops = shops;
	}









	public void addShops() {
		this.shopservice.addComment(shops);
	}
}
