package entity;

import java.io.Serializable;

public class user implements Serializable{

	   private int usersId;
	   private String  image;
	   private String usersName;
	   private String usersLastname ;

	public int getUsersId() {
		return usersId;
	}

	public void setUsersId(int usersId) {
		this.usersId = usersId;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getUsersName() {
		return usersName;
	}

	public void setUsersName(String usersName) {
		this.usersName = usersName;
	}

	public String getUsersLastname() {
		return usersLastname;
	}

	public void setUsersLastname(String usersLastname) {
		this.usersLastname = usersLastname;
	}

	@Override
	public String toString() {
		return "user [usersId=" + usersId + ", image=" + image + ", usersName=" + usersName + ", usersLastname="
				+ usersLastname + "]";
	}
	   
	   
}
