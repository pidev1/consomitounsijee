package entity;

import java.io.Serializable;
import java.util.Date;

public class Product implements Serializable {

	
	private String Image;
	private Date DateProd;
	private String Libelle;
	private String  Description;
	private String Name;
	private int Price;
	private int ProductId;
	private int Quantity;
	
	public Product() {
		super();
	}

	public Product(String image, Date dateProd, String libelle, String description, String name, int price,
			int productId, int quantity, Category mycotegory) {
		super();
		Image = image;
		DateProd = dateProd;
		Libelle = libelle;
		Description = description;
		Name = name;
		Price = price;
		ProductId = productId;
		Quantity = quantity;
		Mycotegory = mycotegory;
	}

	public void setPrice(int price) {
		Price = price;
	}

	@Override
	public String toString() {
		return "Product [Image=" + Image + ", DateProd=" + DateProd + ", Libelle=" + Libelle + ", Description="
				+ Description + ", Name=" + Name + ", Price=" + Price + ", ProductId=" + ProductId + ", Quantity="
				+ Quantity + ", Mycotegory=" + Mycotegory + "]";
	}

	public String getImage() {
		return Image;
	}

	public void setImage(String image) {
		Image = image;
	}

	public Date getDateProd() {
		return DateProd;
	}

	public void setDateProd(Date dateProd) {
		DateProd = dateProd;
	}

	public String getLibelle() {
		return Libelle;
	}

	public void setLibelle(String libelle) {
		Libelle = libelle;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public int getPrice() {
		return Price;
	}

	
	public int getProductId() {
		return ProductId;
	}

	public void setProductId(int productId) {
		ProductId = productId;
	}

	public int getQuantity() {
		return Quantity;
	}

	public void setQuantity(int quantity) {
		Quantity = quantity;
	}

	public Category getMycotegory() {
		return Mycotegory;
	}

	public void setMycotegory(Category mycotegory) {
		Mycotegory = mycotegory;
	}

	private Category Mycotegory;
}
