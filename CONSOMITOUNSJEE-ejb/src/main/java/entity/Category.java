package entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Category implements Serializable  {
	
	public Category() {
		super();
	}
	@Override
	public String toString() {
		return "Category [Name=" + Name + ", CategoryId=" + CategoryId + ", Listproduct=" + Listproduct + "]";
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public int getCategoryId() {
		return CategoryId;
	}
	public void setCategoryId(int categoryId) {
		CategoryId = categoryId;
	}
	public List<Product> getListproduct() {
		return Listproduct;
	}
	public void setListproduct(List<Product> listproduct) {
		Listproduct = listproduct;
	}
	private String Name;
	private int CategoryId ;
	private List<Product> Listproduct = new ArrayList<Product>() ;

}
