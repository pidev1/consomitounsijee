package entity;

import java.io.Serializable;
import java.util.Date;

public class Claims implements Serializable{
	
	public Claims() {
		super();
	}
	public Claims(int claimsId, Date claimsDate, String description, String claimsState, String reference,
			String username, String type, user myuser, Product myproduct) {
		super();
		ClaimsId = claimsId;
		ClaimsDate = claimsDate;
		Description = description;
		ClaimsState = claimsState;
		Reference = reference;
		Username = username;
		Type = type;
		Myuser = myuser;
		Myproduct = myproduct;
	}
	public int getClaimsId() {
		return ClaimsId;
	}
	public void setClaimsId(int claimsId) {
		ClaimsId = claimsId;
	}
	public Date getClaimsDate() {
		return ClaimsDate;
	}
	public void setClaimsDate(Date claimsDate) {
		ClaimsDate = claimsDate;
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}
	public String getClaimsState() {
		return ClaimsState;
	}
	public void setClaimsState(String claimsState) {
		ClaimsState = claimsState;
	}
	public String getReference() {
		return Reference;
	}
	public void setReference(String reference) {
		Reference = reference;
	}
	public String getUsername() {
		return Username;
	}
	public void setUsername(String username) {
		Username = username;
	}
	public String getType() {
		return Type;
	}
	public void setType(String type) {
		Type = type;
	}
	public user getMyuser() {
		return Myuser;
	}
	public void setMyuser(user myuser) {
		Myuser = myuser;
	}
	public Product getMyproduct() {
		return Myproduct;
	}
	public void setMyproduct(Product myproduct) {
		Myproduct = myproduct;
	}
	@Override
	public String toString() {
		return "Claims [ClaimsId=" + ClaimsId + ", ClaimsDate=" + ClaimsDate + ", Description=" + Description
				+ ", ClaimsState=" + ClaimsState + ", Reference=" + Reference + ", Username=" + Username + ", Type="
				+ Type + ", Myuser=" + Myuser + ", Myproduct=" + Myproduct + "]";
	}
	private int ClaimsId;
	private Date ClaimsDate;
	private String Description;
	private String  ClaimsState ;
	private String Reference;
	private String Username;
	private String Type;
	private user Myuser;
	private Product Myproduct;
	
	

}
