package entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

 
 

public class Publish implements Serializable {
 
//	@JsonProperty("id")
	private int publishId;
	 
	public Publish() {
		super();
	}

	private String subjet;
	 
	private Date datePublish;

	private user myUsers;
	private int usersId;

	public List<PublishComent> getLstcomments() {
		return lstcomments;
	}

	public void setLstcomments(List<PublishComent> lstcomments) {
		this.lstcomments = lstcomments;
	}

	private List<PublishComent> lstcomments = new ArrayList<PublishComent>() ;
	
	public Publish(String subjet, int usersId) {
		super();
		this.subjet = subjet;
		this.usersId = usersId;
	}

	public int getUsersId() {
		return usersId;
	}

	public void setUsersId(int usersId) {
		this.usersId = usersId;
	}

	public int getPublishId() {
		return publishId;
	}

	public void setPublishId(int publishId) {
		this.publishId = publishId;
	}

	public String getSubjet() {
		return subjet;
	}

	public void setSubjet(String subjet) {
		this.subjet = subjet;
	}

 
 

	public user getMyUsers() {
		return myUsers;
	}

	public void setMyUsers(user myUsers) {
		this.myUsers = myUsers;
	}

	public Date getDatePublish() {
		return datePublish;
	}

	public void setDatePublish(Date datePublish) {
		this.datePublish = datePublish;
	}

	@Override
	public String toString() {
		return "Publish [publishId=" + publishId + ", subjet=" + subjet + ", datePublish=" + datePublish + ", myUsers="
				+ myUsers + ", usersId=" + usersId + "]";
	}

 
	
	
}
