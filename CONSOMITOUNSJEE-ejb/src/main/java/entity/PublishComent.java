package entity;

import java.io.Serializable;
import java.util.Date;

public class PublishComent implements Serializable{

	  private int publishComentId ;
 
	  private String content ;
      private Date dateCreation  ;
      private user myUsers ;
private int publishId;
      private Publish publish;
      
      public PublishComent(Publish publish) {
		super();
		this.publish = publish;
	}



	public Publish getPublish() {
		return publish;
	}



	public void setPublish(Publish publish) {
		this.publish = publish;
	}



	public int getPublishId() {
		return publishId;
	}



	public void setPublishId(int publishId) {
		this.publishId = publishId;
	}



	public PublishComent() {
		super();
	}



	public int getPublishComentId() {
		return publishComentId;
	}



	public void setPublishComentId(int publishComentId) {
		this.publishComentId = publishComentId;
	}



	public String getContent() {
		return content;
	}



	public void setContent(String content) {
		this.content = content;
	}



	public Date getDateCreation() {
		return dateCreation;
	}



	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}



	public user getMyUsers() {
		return myUsers;
	}



	public void setMyUsers(user myUsers) {
		this.myUsers = myUsers;
	}



	 


	public PublishComent(int publishComentId, String content, Date dateCreation, user myUsers) {
		super();
		this.publishComentId = publishComentId;
		this.content = content;
		this.dateCreation = dateCreation;
		this.myUsers = myUsers;
	}



	@Override
	public String toString() {
		return "PublishComent [publishComentId=" + publishComentId + ", content=" + content + ", dateCreation="
				+ dateCreation + ", myUsers=" + myUsers.getUsersId() + ", publish=" + publish.getPublishId() + "]";
	}

	 
      
      
}
