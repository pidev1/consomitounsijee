package entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Delivery  implements Serializable {

	private int DeliveryId;
	private Date DeliveryDate;
	private String DeliveryState;
	private String  lng ;
	private String ing;
	private String Pays;
	private String Ville;
	private String Adress;
	private user Myuser;
	private Order Myoder;
	public user getMyuser() {
		return Myuser;
	}

	public void setMyuser(user myuser) {
		Myuser = myuser;
	}

	public Order getMyoder() {
		return Myoder;
	}

	public void setMyoder(Order myoder) {
		Myoder = myoder;
	}

	private List<DeliveryMan> listDeliveryMan = new ArrayList<DeliveryMan>() ;

	public Delivery() {
		super();
	}

	
	

	public Delivery(int deliveryId, Date deliveryDate, String deliveryState, String lng, String ing, String pays,
			String ville, String adress, user myuser, Order myoder) {
		super();
		DeliveryId = deliveryId;
		DeliveryDate = deliveryDate;
		DeliveryState = deliveryState;
		this.lng = lng;
		this.ing = ing;
		Pays = pays;
		Ville = ville;
		Adress = adress;
		Myuser = myuser;
		Myoder = myoder;
	}

	@Override
	public String toString() {
		return "Delivery [DeliveryId=" + DeliveryId + ", DeliveryDate=" + DeliveryDate + ", DeliveryState="
				+ DeliveryState + ", lng=" + lng + ", ing=" + ing + ", Pays=" + Pays + ", Ville=" + Ville + ", Adress="
				+ Adress + ", Myuser=" + Myuser + ", Myoder=" + Myoder + ", listDeliveryMan=" + listDeliveryMan + "]";
	}

	public int getDeliveryId() {
		return DeliveryId;
	}

	public void setDeliveryId(int deliveryId) {
		DeliveryId = deliveryId;
	}

	public Date getDeliveryDate() {
		return DeliveryDate;
	}

	public void setDeliveryDate(Date deliveryDate) {
		DeliveryDate = deliveryDate;
	}

	public String getDeliveryState() {
		return DeliveryState;
	}

	public void setDeliveryState(String deliveryState) {
		DeliveryState = deliveryState;
	}

	public String getLng() {
		return lng;
	}

	public void setLng(String lng) {
		this.lng = lng;
	}

	public String getIng() {
		return ing;
	}

	public void setIng(String ing) {
		this.ing = ing;
	}

	public String getPays() {
		return Pays;
	}

	public void setPays(String pays) {
		Pays = pays;
	}

	public String getVille() {
		return Ville;
	}

	public void setVille(String ville) {
		Ville = ville;
	}

	public String getAdress() {
		return Adress;
	}

	public void setAdress(String adress) {
		Adress = adress;
	}

	public List<DeliveryMan> getListDeliveryMan() {
		return listDeliveryMan;
	}

	public void setListDeliveryMan(List<DeliveryMan> listDeliveryMan) {
		this.listDeliveryMan = listDeliveryMan;
	}
	
	
	
}
