package entity;

import java.io.Serializable;
import java.util.Date;

public class Shops implements Serializable {
	
	private int ShopId;
	private Date DateShop;
	private String NameShop;
	private String  DescriptionShop;
	public int getShopId() {
		return ShopId;
	}
	public void setShopId(int shopId) {
		ShopId = shopId;
	}
	public Date getDateShop() {
		return DateShop;
	}
	public void setDateShop(Date dateShop) {
		DateShop = dateShop;
	}
	public String getNameShop() {
		return NameShop;
	}
	public void setNameShop(String nameShop) {
		NameShop = nameShop;
	}
	public String getDescriptionShop() {
		return DescriptionShop;
	}
	public void setDescriptionShop(String descriptionShop) {
		DescriptionShop = descriptionShop;
	}
	@Override
	public String toString() {
		return "Shops [ShopId=" + ShopId + ", DateShop=" + DateShop + ", NameShop=" + NameShop + ", DescriptionShop="
				+ DescriptionShop + "]";
	}
	public Shops(int shopId, Date dateShop, String nameShop, String descriptionShop) {
		super();
		ShopId = shopId;
		DateShop = dateShop;
		NameShop = nameShop;
		DescriptionShop = descriptionShop;
	}
	public Shops() {
		super();
	}
	

}
