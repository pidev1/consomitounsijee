package entity;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class DeliveryMan implements Serializable{

	
	
	private int DeliveryManId;
	private String UsersName_d;
	private String UsersLastname_d;
	private String UsersEmail_d;
	private String UsersCin_d;
	private String UsersAdress_d;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
	private Date DateRegister_d;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
	private Date DateNaissance_d;
	private String Pays_d;
	private String Ville_d;
	private String State_d;
	private String Phone_d;
	private String ImageCin_d;
	private String lng;
	private String ing;
	
	private Delivery delivery;
	public DeliveryMan() {
		super();
	}

	public DeliveryMan(int deliveryManId, String usersName_d, String usersLastname_d, String usersEmail_d,
			String usersCin_d, String usersAdress_d, Date dateRegister_d, Date dateNaissance_d, String pays_d,
			String ville_d, String state_d, String phone_d, String imageCin_d, String lng, String ing,
			Delivery delivery) {
		super();
		DeliveryManId = deliveryManId;
		UsersName_d = usersName_d;
		UsersLastname_d = usersLastname_d;
		UsersEmail_d = usersEmail_d;
		UsersCin_d = usersCin_d;
		UsersAdress_d = usersAdress_d;
		DateRegister_d = dateRegister_d;
		DateNaissance_d = dateNaissance_d;
		Pays_d = pays_d;
		Ville_d = ville_d;
		State_d = state_d;
		Phone_d = phone_d;
		ImageCin_d = imageCin_d;
		this.lng = lng;
		this.ing = ing;
		this.delivery = delivery;
	}

	@Override
	public String toString() {
		return "DeliveryMan [DeliveryManId=" + DeliveryManId + ", UsersName_d=" + UsersName_d + ", UsersLastname_d="
				+ UsersLastname_d + ", UsersEmail_d=" + UsersEmail_d + ", UsersCin_d=" + UsersCin_d + ", UsersAdress_d="
				+ UsersAdress_d + ", DateRegister_d=" + DateRegister_d + ", DateNaissance_d=" + DateNaissance_d
				+ ", Pays_d=" + Pays_d + ", Ville_d=" + Ville_d + ", State_d=" + State_d + ", Phone_d=" + Phone_d
				+ ", ImageCin_d=" + ImageCin_d + ", lng=" + lng + ", ing=" + ing + ", delivery=" + delivery + "]";
	}

	public int getDeliveryManId() {
		return DeliveryManId;
	}

	public void setDeliveryManId(int deliveryManId) {
		DeliveryManId = deliveryManId;
	}

	public String getUsersName_d() {
		return UsersName_d;
	}

	public void setUsersName_d(String usersName_d) {
		UsersName_d = usersName_d;
	}

	public String getUsersLastname_d() {
		return UsersLastname_d;
	}

	public void setUsersLastname_d(String usersLastname_d) {
		UsersLastname_d = usersLastname_d;
	}

	public String getUsersEmail_d() {
		return UsersEmail_d;
	}

	public void setUsersEmail_d(String usersEmail_d) {
		UsersEmail_d = usersEmail_d;
	}

	public String getUsersCin_d() {
		return UsersCin_d;
	}

	public void setUsersCin_d(String usersCin_d) {
		UsersCin_d = usersCin_d;
	}

	public String getUsersAdress_d() {
		return UsersAdress_d;
	}

	public void setUsersAdress_d(String usersAdress_d) {
		UsersAdress_d = usersAdress_d;
	}

	public Date getDateRegister_d() {
		return DateRegister_d;
	}

	public void setDateRegister_d(Date dateRegister_d) {
		DateRegister_d = dateRegister_d;
	}

	public Date getDateNaissance_d() {
		return DateNaissance_d;
	}

	public void setDateNaissance_d(Date dateNaissance_d) {
		DateNaissance_d = dateNaissance_d;
	}

	public String getPays_d() {
		return Pays_d;
	}

	public void setPays_d(String pays_d) {
		Pays_d = pays_d;
	}

	public String getVille_d() {
		return Ville_d;
	}

	public void setVille_d(String ville_d) {
		Ville_d = ville_d;
	}

	public String getState_d() {
		return State_d;
	}

	public void setState_d(String state_d) {
		State_d = state_d;
	}

	public String getPhone_d() {
		return Phone_d;
	}

	public void setPhone_d(String phone_d) {
		Phone_d = phone_d;
	}

	public String getImageCin_d() {
		return ImageCin_d;
	}

	public void setImageCin_d(String imageCin_d) {
		ImageCin_d = imageCin_d;
	}

	public String getLng() {
		return lng;
	}

	public void setLng(String lng) {
		this.lng = lng;
	}

	public String getIng() {
		return ing;
	}

	public void setIng(String ing) {
		this.ing = ing;
	}

	public Delivery getDelivery() {
		return delivery;
	}

	public void setDelivery(Delivery delivery) {
		this.delivery = delivery;
	}

	
}
