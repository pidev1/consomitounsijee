package entity;

import java.io.Serializable;
import java.util.Date;

public class Order implements Serializable {
	
	
	private Date OrderDate;
	private String OrderState;
	
	
	private int OrderId;
	
	public Order() {
		super();
	}
	public Order(Date orderDate, String orderState, int orderId, user myuser, Product myproduct) {
		super();
		OrderDate = orderDate;
		OrderState = orderState;
		OrderId = orderId;
		Myuser = myuser;
		Myproduct = myproduct;
	}
	@Override
	public String toString() {
		return "Order [OrderDate=" + OrderDate + ", OrderState=" + OrderState + ", OrderId=" + OrderId + ", Myuser="
				+ Myuser + ", Myproduct=" + Myproduct + "]";
	}
	public Date getOrderDate() {
		return OrderDate;
	}
	public void setOrderDate(Date orderDate) {
		OrderDate = orderDate;
	}
	public String getOrderState() {
		return OrderState;
	}
	public void setOrderState(String orderState) {
		OrderState = orderState;
	}
	public int getOrderId() {
		return OrderId;
	}
	public void setOrderId(int orderId) {
		OrderId = orderId;
	}
	public user getMyuser() {
		return Myuser;
	}
	public void setMyuser(user myuser) {
		Myuser = myuser;
	}
	public Product getMyproduct() {
		return Myproduct;
	}
	public void setMyproduct(Product myproduct) {
		Myproduct = myproduct;
	}
	private user Myuser;
	private Product Myproduct;

}
