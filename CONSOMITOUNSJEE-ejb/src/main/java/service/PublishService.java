package service;

import java.io.StringReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.json.Json;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonReader;
import javax.json.JsonWriter;
import javax.json.JsonObject;
import javax.persistence.EntityManager;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import entity.Publish;
import entity.PublishComent;
import entity.user; 

/**
 * Session Bean implementation class PublishService
 */
@Stateless
@LocalBean
public class PublishService  {

	public String GlobalEndPoint = "localhost:40740";
    /**
     * Default constructor. 
     */
    public PublishService() {
         
    }
    
    
	public List<Publish> GetAllPublish()  {
		List<Publish> PublishList = new ArrayList<Publish>();
		Client client = ClientBuilder.newClient();
		WebTarget web = client.target("http://" + GlobalEndPoint + "/test/test");
		Response response = web.request().get();
		String result = response.readEntity(String.class);
		JsonReader jsonReader = Json.createReader(new StringReader(result));
		JsonArray object = jsonReader.readArray();
		DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
		 
		for (int i = 0; i < object.size(); i++) {
			Publish myProduct = new Publish();
			myProduct.setPublishId(object.getJsonObject(i).getInt("publishId"));
			myProduct.setSubjet(object.getJsonObject(i).getString("subjet"));
		
			
			Date cartDate = null;
		 
			try {

				String jsonDate = object.getJsonObject(i).getString("datePublish");
				SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
				cartDate = formatDate.parse(jsonDate);

			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			myProduct.setDatePublish(cartDate);
			
			 JsonObject userj = (JsonObject) object.getJsonObject(i).get("myUsers");
			 
			
			user u = new user();
			u.setUsersId(object.getJsonObject(i).getInt("usersId"));
			u.setUsersLastname(userj.getString("usersLastname"));
			u.setImage(userj.getString("image"));
			u.setUsersName(userj.getString("usersName"));
			myProduct.setMyUsers(u);
			
			myProduct.setLstcomments(getCommentByPub(myProduct.getPublishId()));
			PublishList.add(myProduct);
		}
		Collections.reverse(PublishList);
		return PublishList;
	}
	
	public List<PublishComent> getCommentByPub(int id){
		List<PublishComent> PublishList = new ArrayList<PublishComent>();
		Client client = ClientBuilder.newClient();
		WebTarget web = client.target("http://" + GlobalEndPoint + "/test/comment/"+id);
		Response response = web.request().get();
		String result = response.readEntity(String.class);
		JsonReader jsonReader = Json.createReader(new StringReader(result));
		JsonArray object = jsonReader.readArray();
		for (int i = 0; i < object.size(); i++) {
			PublishComent publishComent = new PublishComent();
			publishComent.setPublishComentId(object.getJsonObject(i).getInt("publishComentId"));
			publishComent.setMyUsers(null);
			publishComent.setContent(object.getJsonObject(i).getString("content"));
			Date cartDate = null;			 
			try {
				String jsonDate = object.getJsonObject(i).getString("dateCreation");
				SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
				cartDate = formatDate.parse(jsonDate);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			user u = this.getUser(object.getJsonObject(i).getInt("usersId"));
			publishComent.setMyUsers(u);
			publishComent.setDateCreation(cartDate);
			PublishList.add(publishComent);
		}	
		return PublishList;
	}
	public user getUser(int id) {
		Client client = ClientBuilder.newClient();
		WebTarget web = client.target("http://" + GlobalEndPoint + "/test/getUser/"+id);
		Response response = web.request().get();
		String result = response.readEntity(String.class);
		JsonReader jsonReader = Json.createReader(new StringReader(result));
		JsonObject object =	jsonReader.readObject();
		
		user u = new user();
		u.setUsersId(id);
		u.setUsersLastname(object.getString("usersLastname"));
			u.setImage(object.getString("image"));
			u.setUsersName(object.getString("usersName"));
		 
		 
		return u;
	}
	
	public void addComment(PublishComent coment,int id ,int idpub) {
		Client client = ClientBuilder.newClient();
		WebTarget web = client.target("http://" + GlobalEndPoint + "/test/addComment/"+idpub+"/"+id);
		Response res=(Response) web.request().post(Entity.json(coment));
 
		
	}
}
