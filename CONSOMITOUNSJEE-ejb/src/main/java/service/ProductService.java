package service;

import java.io.StringReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonReader;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import entity.DeliveryMan;
import entity.Product;

/**
 * Session Bean implementation class ProductService
 */
@Stateless
@LocalBean
public class ProductService {

    /**
     * Default constructor. 
     */
	
	public String GlobalEndPoint = "localhost:40740";
    public ProductService() {
        // TODO Auto-generated constructor stub
    }
    
    
    
    public List<Product> GetAllProduct()  {
		List<Product>productList = new ArrayList<Product>();
		Client client = ClientBuilder.newClient();
		WebTarget web = client.target("http://" + GlobalEndPoint + "/api/Products");
		Response response = web.request().get();
		String result = response.readEntity(String.class);
		JsonReader jsonReader = Json.createReader(new StringReader(result));
		JsonArray object = jsonReader.readArray();
		DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
		
		for (int i = 0; i < object.size(); i++) {
			Product prod= new Product();
			prod.setProductId(object.getJsonObject(i).getInt("ProductId"));
			prod.setName(object.getJsonObject(i).getString("Name"));
			prod.setDescription(object.getJsonObject(i).getString("Description"));
			prod.setPrice(object.getJsonObject(i).getInt("Price"));
			//prod.setQuantity(object.getJsonObject(i).getInt("Quantity"));
			
			//prod.setImage(object.getJsonObject(i).getString("Image"));
			
			productList.add(prod);
		}
		
		return productList;
		
		
    }

}
