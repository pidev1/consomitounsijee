package service;

import java.io.StringReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import entity.Delivery;
import entity.DeliveryMan;
import entity.Publish;
import entity.Shops;
import entity.user;

/**
 * Session Bean implementation class DeliveyManService
 */
@Stateless
@LocalBean
public class DeliveyManService {

	public String GlobalEndPoint = "localhost:40740";
    /**
     * Default constructor. 
     */
    public DeliveyManService() {
        // TODO Auto-generated constructor stub
    }
    
    
    public List<DeliveryMan> GetAllDeliveryMan()  {
		List<DeliveryMan> DeliveryManList = new ArrayList<DeliveryMan>();
		Client client = ClientBuilder.newClient();
		WebTarget web = client.target("http://" + GlobalEndPoint + "/api/DeliveryMen");
		Response response = web.request().get();
		String result = response.readEntity(String.class);
		JsonReader jsonReader = Json.createReader(new StringReader(result));
		JsonArray object = jsonReader.readArray();
		DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
		
		for (int i = 0; i < object.size(); i++) {
			DeliveryMan deliveym= new DeliveryMan();
			deliveym.setDeliveryManId(object.getJsonObject(i).getInt("DeliveryManId"));
			deliveym.setUsersLastname_d(object.getJsonObject(i).getString("UsersLastname_d"));
				deliveym.setUsersName_d(object.getJsonObject(i).getString("UsersName_d"));
		//	deliveym.setUsersAdress_d(object.getJsonObject(i).getString("UsersAdress_d"));
			deliveym.setUsersCin_d(object.getJsonObject(i).getString("UsersCin_d"));
			//deliveym.setUsersEmail_d(object.getJsonObject(i).getString("UsersEmail_d"));
			//deliveym.setState_d(object.getJsonObject(i).getString("State_d"));
			//deliveym.setVille_d(object.getJsonObject(i).getString("Ville_d"));
			Date cartDate = null;
			 
			try {

				String jsonDate = object.getJsonObject(i).getString("DateRegister_d");
				SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
				cartDate = formatDate.parse(jsonDate); 

			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
				deliveym.setDateRegister_d(cartDate);
			//	 JsonObject Mydelivery = (JsonObject) object.getJsonObject(i).get("MyDelivery");
			 
				
			/*	Delivery u = new Delivery();
				u.setDeliveryId(object.getJsonObject(i).getInt("DeliveryId"));
				deliveym.setDelivery(u);
				*/
				DeliveryManList.add(deliveym);
		}
		
		
		return DeliveryManList;
    }
    
    public void addDelivryMan(DeliveryMan deliveryMan) {
  
		Client client = ClientBuilder.newClient();
		WebTarget web = client.target("http://" + GlobalEndPoint + "/api/DeliveryMen");
		Response res=(Response) web.request().post(Entity.json(deliveryMan));
		
		
    	System.out.println(deliveryMan+"test");
    }
    
    
  
}
