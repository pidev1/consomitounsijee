package service;

import java.io.StringReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonReader;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import entity.DeliveryMan;
import entity.Shops;

/**
 * Session Bean implementation class ShopsService
 */
@Stateless
@LocalBean
public class ShopsService {

    /**
     * Default constructor. 
     */
	
	public String GlobalEndPoint = "localhost:40740";
    public ShopsService() {
        // TODO Auto-generated constructor stub
    }

    public List<Shops> GetAllShops()  {
		List<Shops>listshops = new ArrayList<Shops>();
		Client client = ClientBuilder.newClient();
		WebTarget web = client.target("http://" + GlobalEndPoint + "/api/Shops");
		Response response = web.request().get();
		String result = response.readEntity(String.class);
		JsonReader jsonReader = Json.createReader(new StringReader(result));
		JsonArray object = jsonReader.readArray();
		DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
		for (int i = 0; i < object.size(); i++) {
			Shops shp= new Shops();
			
			shp.setShopId(object.getJsonObject(i).getInt("ShopId"));
			shp.setDescriptionShop(object.getJsonObject(i).getString("DescriptionShop"));
			shp.setNameShop(object.getJsonObject(i).getString("NameShop"));
			
			Date cartDate = null;
			 
			try {

				String jsonDate = object.getJsonObject(i).getString("DescriptionShop");
				SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
				cartDate = formatDate.parse(jsonDate); 

			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
				shp.setDateShop(cartDate);
		     listshops.add(shp);
		}
		return listshops;
    }
    
    public void addComment(Shops shops) {
  	  
		Client client = ClientBuilder.newClient();
		WebTarget web = client.target("http://" + GlobalEndPoint + "/api/Shops");
		Response res=(Response) web.request().post(Entity.json(shops));
		
		
    	System.out.println(shops+"test");
    }
}
